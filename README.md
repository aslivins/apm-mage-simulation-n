# Apm Mage Simulation N



## Abstract
Hearthstone is an online card game in which games are played between two players with
pre-made decks of thirty cards from their individual collection. To maximize their win
percentage, it is advantageous to players to seek out the most optimal deck list of cards.
A specific deck archetype, Ignite Mage, follows the game plan of assembling a number of
combo pieces and executing a strict order of play to attempt to win the game on a single
turn. If the combo succeeds, the player wins the game, and if the combo fails, the player
loses the game. When the combo turn is executed, the outcome of a win or loss follows
a Bernoulli distribution with parameter p. The purpose of this project is to answer two
questions: By using Python to simulate large amounts of the execution of the combo turn,
which are essentially Bernoulli trials, can we find the optimal deck list of cards, and can we
estimate the Bernoulli parameter p given resources, or combo pieces, available?

## Significance
Many deck archetypes in Hearthstone follow a similar strategy of executing a combo to
win on a single turn. The framework that is developed for simulating the combo turn of
Ignite Mage can be applied to other archetypes to be similarly analyzed. The conclusions of
the project can influence the player community to adopt one choice of decklist over another
if publicly stated, or can be used to gain an advantage over the rest of the community if kept
private.

## Project Method:

1) Develop a Python program that can model and simulate a combo turn of the game, using a class object for the cards.
2) Run large simulations of different implementations of the model, to answer the two goal questions


## Project Deliverables: 
Answer to both of the two questions, the first being that 1 Molten 1 Ignite is the strongest of the four lists; the second being a spreadsheet of the true Win percentage given initial conditions.

Spreadsheet link (last tab has best formatted information):
https://docs.google.com/spreadsheets/d/1TmeijcfInsEOKoFL-dn9FX_deD2SE2RuBOvdq8jB5Is/edit?usp=sharing
