import random
 
class Cards:
    ELEMENTAL_EVO = 0
    HOT_STREAK = 1
    FIRST_FLAME = 2
    SECOND_FLAME = 2.5
    IGNITE = 3
    BISCUIT = 4
    ICE_BLOCK = 5
    MOLTEN = 6
    # Costs with 1 sorc on board
    COSTS = {
        ELEMENTAL_EVO : 0,
        HOT_STREAK : 0,
        FIRST_FLAME : 0,
        SECOND_FLAME: 0,
        IGNITE : 1,
        BISCUIT : 1,
        ICE_BLOCK : 2,
        MOLTEN : 3
    }
    @staticmethod
    def get_cost(card, played_order):
        if card == Cards.MOLTEN and len(played_order) > 0 and played_order[-1] == Cards.HOT_STREAK:
            return max(Cards.COSTS.get(Cards.MOLTEN) - 2, 0)
        #if card == Cards.IGNITE and len(played_order) > 0 and played_order[-1] == Cards.HOT_STREAK:
            #return max(Cards.COSTS.get(Cards.IGNITE) - 1, 0)
        return Cards.COSTS.get(card)
        
 
def draw(cards_in_hand, cards_in_deck):
    card = cards_in_deck.pop(0)
    cards_in_hand.append(card)
 
"""
Initial state: 1 chandler, 1 sorc, 1 mana spare
Goal: cast molten on sorc
 
"""
 
def run(mana, igniteCounter, cards_in_hand, cards_in_deck):
    play_order = []
    while True:
 
        # No more mana left and haven't played molten.
        # Not possible to win anymore.
        if mana == 0:
            return False

        #if 10 ignites have been played, the game is most likely over
        if igniteCounter > 7:
            return True, play_order

        #Molten reflection sets flame spells to zero, so I set mana = 100
        #If molten reflection is cast (mana > 50) and ignite is in hand, first priority
        if Cards.IGNITE in cards_in_hand and mana > 50:
            play_order.append(Cards.IGNITE)
            cards_in_hand.remove(Cards.IGNITE)
            cards_in_deck.append(Cards.IGNITE)
            random.shuffle(cards_in_deck)
            draw(cards_in_hand, cards_in_deck)
            igniteCounter += 1
            continue
            
 
        if Cards.HOT_STREAK in cards_in_hand and Cards.MOLTEN in cards_in_hand:
            play_order.append(Cards.HOT_STREAK)
            cards_in_hand.remove(Cards.HOT_STREAK)
            if len(cards_in_deck) == 0:
                continue
            draw(cards_in_hand, cards_in_deck)
            continue
            
        
        if Cards.MOLTEN in cards_in_hand:
            #takes into account hot streak discount
            cost = Cards.get_cost(Cards.MOLTEN, play_order)
            if mana >= cost:
                play_order.append(Cards.MOLTEN)
                cards_in_hand.remove(Cards.MOLTEN)
                mana += 100
                if len(cards_in_deck) == 0:
                    continue
                draw(cards_in_hand, cards_in_deck)
                continue

        #Moved this part up because ignite should be played after a hot streak if no molten
        #If we have ignite and at least 1 mana to spare after, we cast the ignite to draw hotstreak+molten
        if Cards.IGNITE in cards_in_hand:
            cost = Cards.get_cost(Cards.IGNITE, play_order)
            if mana >= cost + 1:
                play_order.append(Cards.IGNITE)
                cards_in_hand.remove(Cards.IGNITE)
                mana -= cost
 
                #print(f"Before: {cards_in_hand} {cards_in_deck}")
                cards_in_deck.append(Cards.IGNITE)
                random.shuffle(cards_in_deck)
                draw(cards_in_hand, cards_in_deck)
                #print(f"After: {cards_in_hand} {cards_in_deck}")
                continue
        
        if len(cards_in_deck) == 0:
            #print(f"Ran out of cards. Hand: {cards_in_hand}. Mana: {mana}.")
            return False, play_order
 
        if Cards.BISCUIT in cards_in_hand:
            cost = Cards.get_cost(Cards.BISCUIT, play_order)
            if mana >= cost:
                play_order.append(Cards.BISCUIT)
                cards_in_hand.remove(Cards.BISCUIT)
                mana = mana - cost + 2
 
                continue
 
        if Cards.FIRST_FLAME in cards_in_hand:
            cost = Cards.get_cost(Cards.FIRST_FLAME, play_order)
            if mana >= cost:
                play_order.append(Cards.FIRST_FLAME)
                cards_in_hand.remove(Cards.FIRST_FLAME)
                cards_in_hand.append(Cards.SECOND_FLAME)
                mana -= cost
 
                #print(f"Before: {cards_in_hand} {cards_in_deck}")
                draw(cards_in_hand, cards_in_deck)
                #print(f"After: {cards_in_hand} {cards_in_deck}")
                continue
        
        if Cards.SECOND_FLAME in cards_in_hand:
            cost = Cards.get_cost(Cards.SECOND_FLAME, play_order)
            if mana >= cost:
                play_order.append(Cards.SECOND_FLAME)
                cards_in_hand.remove(Cards.SECOND_FLAME)
                mana -= cost
 
                draw(cards_in_hand, cards_in_deck)
                continue
        
        if Cards.ELEMENTAL_EVO in cards_in_hand:
            cost = Cards.get_cost(Cards.ELEMENTAL_EVO, play_order)
            if mana >= cost:
                play_order.append(Cards.ELEMENTAL_EVO)
                cards_in_hand.remove(Cards.ELEMENTAL_EVO)
                mana -= cost
 
                draw(cards_in_hand, cards_in_deck)
                continue
        
        
        # If hot streak all we have, and no molten, pray we draw molten
        if Cards.HOT_STREAK in cards_in_hand:
            cost = Cards.get_cost(Cards.HOT_STREAK, play_order)
            if mana >= cost:
                play_order.append(Cards.HOT_STREAK)
                cards_in_hand.remove(Cards.HOT_STREAK)
                mana -= cost
 
                draw(cards_in_hand, cards_in_deck)
                continue
        
        #Out of fire spells
        return False, play_order
 
 
def main():
    runs = 1000000
    successes = 0
 
    #LEGEND
    #cards_nX: 2 elemental evocations in deck
    #1: 2 molten 2 ignite
    #2: 1 molten 2 ignite
    #3: 2 molten 1 ignite
    #4: 1 molten 1 ignite
 
    cards_1 = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.IGNITE, Cards.MOLTEN, Cards.MOLTEN]
    cards_2 = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.IGNITE, Cards.MOLTEN]
    cards_3 = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.MOLTEN, Cards.MOLTEN]
    cards_4 = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.MOLTEN]
    
    cards_1X = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.IGNITE, Cards.MOLTEN, Cards.MOLTEN]
    cards_2X = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.IGNITE, Cards.MOLTEN]
    cards_3X = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.MOLTEN, Cards.MOLTEN]
    cards_4X = [Cards.HOT_STREAK, Cards.HOT_STREAK, Cards.ELEMENTAL_EVO, Cards.ELEMENTAL_EVO, Cards.FIRST_FLAME, Cards.FIRST_FLAME,
               Cards.IGNITE, Cards.MOLTEN]

    

    
    for j in range(1, 6):
        print(f"Testing {j} spell(s) in hand\n")
        #Test for cards_1
        for i in range(runs):
            cards_ = cards_1X.copy()
            random.shuffle(cards_)
            cards_in_hand = cards_[:j]
            cards_in_deck = cards_[j:]
            cards_in_deck.append(Cards.ICE_BLOCK)
            cards_in_deck.append(Cards.ICE_BLOCK)
            random.shuffle(cards_in_deck)
            result, played_order = run(1, 0, cards_in_hand, cards_in_deck)
            #print("Result: ", result)
         
            if result:
                successes += 1

        print(f"Testing 2 Molten 2 Ignite");
        print(f"{successes}/{runs} passed: {successes*100/runs}%.")
        runs = 1000000
        successes = 0
        #Test for cards_2
        for i in range(runs):
            cards_ = cards_2X.copy()
            random.shuffle(cards_)
            cards_in_hand = cards_[:j]
            cards_in_deck = cards_[j:]
            cards_in_deck.append(Cards.ICE_BLOCK)
            cards_in_deck.append(Cards.ICE_BLOCK)
            random.shuffle(cards_in_deck)
            result, played_order = run(1, 0, cards_in_hand, cards_in_deck)
            #print("Result: ", result)
         
            if result:
                successes += 1

        print(f"Testing 1 Molten 2 Ignite");
        print(f"{successes}/{runs} passed: {successes*100/runs}%.")
        runs = 1000000
        successes = 0
        #Test for cards_3
        for i in range(runs):
            cards_ = cards_3X.copy()
            random.shuffle(cards_)
            cards_in_hand = cards_[:j]
            cards_in_deck = cards_[j:]
            cards_in_deck.append(Cards.ICE_BLOCK)
            cards_in_deck.append(Cards.ICE_BLOCK)
            random.shuffle(cards_in_deck)
            result, played_order = run(1, 0, cards_in_hand, cards_in_deck)
            #print("Result: ", result)
         
            if result:
                successes += 1

        print(f"Testing 2 Molten 1 Ignite");
        print(f"{successes}/{runs} passed: {successes*100/runs}%.")
        runs = 1000000
        successes = 0
        #Test for cards_4
        for i in range(runs):
            cards_ = cards_4X.copy()
            random.shuffle(cards_)
            cards_in_hand = cards_[:j]
            cards_in_deck = cards_[j:]
            cards_in_deck.append(Cards.ICE_BLOCK)
            cards_in_deck.append(Cards.ICE_BLOCK)
            random.shuffle(cards_in_deck)
            result, played_order = run(1, 0, cards_in_hand, cards_in_deck)
            #print("Result: ", result)
         
            if result:
                successes += 1

        print(f"Testing 1 Molten 1 Ignite");
        print(f"{successes}/{runs} passed: {successes*100/runs}%.")
        runs = 1000000
        successes = 0

        
        print(f"\n----------------------------\n")
        
        




    
 
def test():
    mana = 1
    cards = [Cards.HOT_STREAK, Cards.BISCUIT, Cards.IGNITE, Cards.MOLTEN]
    random.shuffle(cards)
    cards_in_hand = cards[:1]
    cards_in_deck = cards[1:]
 
    print(f"{cards_in_hand} {cards_in_deck}")
    result, played_order = run(mana, cards_in_hand, cards_in_deck)
 
    print(result, played_order)
 
if __name__ == '__main__':
    main()
    #test()
